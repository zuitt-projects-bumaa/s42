// alert("zj!!!!")


// to check what the document holds
console.log(document);

// get document in the HTML element with the id txt-first-name
const txtFirstName = document.querySelector("#txt-first-name")

// alternative way of targeting an element
	// document.getElementById("txt-first-name")
	// document.getElementByClassName()
	// document.getElementByTagName()

// mini activity

const spanFullName = document.querySelector("#span-full-name")

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
});

// keyup is an EVENT
txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target)
	console.log(event.target.value)
});

/*
Common Events:
	1. change
	2. click
	3. load
	4. keydown -keyboard is pressed
	5. keyup -keyboard is released
*/

